function start () {
	$(function() {
		// Show default wechat tab content.
		showTab('wechat');
        
		bindNaviTabEvents();
	});
}

function showTab (tabId) {
	// Hide all content tab.
	$('.content').hide();

	// Show the specified content.
	$('#' + tabId + '-content').show();
}

function bindNaviTabEvents () {
	
	$('.tab-item').click(function () {

		var tabId = $(this).attr('id');

		showTab(tabId);

	});
}

start();
